package com.sisweb.labo2.Repository;

import com.sisweb.labo2.Entities.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface UserRepository extends CrudRepository<User, Integer> {

    @Query("SELECT u FROM User u WHERE u.name LIKE %:name%")
    Iterable<User> findByUserName(@Param("name") String name);

    @Query("SELECT u FROM User u WHERE u.email LIKE %:email%")
    User findByUserEmail(@Param("email") String email);
}
