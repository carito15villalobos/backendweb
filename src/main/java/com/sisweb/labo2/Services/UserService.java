package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.User;

public interface UserService{

    Iterable<User> listAllUsers();

    void saveUser(User user);

    User getUser(Integer id);

    void deleteUser(Integer id);

    Iterable<User> getUserByName(String name);

    User getUserByemail(String email);
}
