package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.Genre;

public interface GenreService {
    Iterable<Genre> listAllGenre();
    void saveGenres(Genre genre);
    void deleteGenre(Integer id);
    Genre getGenre(Integer id);

}
