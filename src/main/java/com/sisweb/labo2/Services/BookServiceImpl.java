package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.Book;
import com.sisweb.labo2.Repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class BookServiceImpl implements BookService{

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    @Qualifier(value = "bookRepository")
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public Iterable<Book> listAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public void saveBooks(Book book) {
        bookRepository.save(book);
    }

    @Override
    public Book getBook(Integer id) {
        return bookRepository.findById(id).get();
    }

    @Override
    public void deleteBook(Integer id) {
        bookRepository.deleteById(id);
    }

    @Override
    public Iterable<Book> getBookByAuthor(String author) {
        return bookRepository.findByAuthor(author);
    }
}
