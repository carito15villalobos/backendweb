package com.sisweb.labo2.Services;

import com.sisweb.labo2.Entities.Book;

public interface BookService {

    Iterable<Book> listAllBooks();

    void saveBooks(Book book);

    Book getBook(Integer id);

    void deleteBook(Integer id);

    Iterable<Book> getBookByAuthor(String author);
}
