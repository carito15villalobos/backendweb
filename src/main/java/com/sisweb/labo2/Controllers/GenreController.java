package com.sisweb.labo2.Controllers;

import com.sisweb.labo2.Entities.Book;
import com.sisweb.labo2.Entities.Genre;
import com.sisweb.labo2.Services.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
@CrossOrigin(origins = "http://localhost:4200")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping(value="/allGenres")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody
    List<Genre> getAllGenres() {
        List<Genre> genres = (List<Genre>) genreService.listAllGenre();
        return genres;
    }


    @PostMapping(value="/newGenre",consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void createGenre( @Valid @RequestBody Genre genre) {
        genreService.saveGenres(genre);
    }


    @DeleteMapping(value="/deleteGenre/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody void deleteGenre(@PathVariable Integer id) {
        genreService.deleteGenre(id);
    }

    @GetMapping(value="/getGenreById/{id}")
    @ResponseStatus(HttpStatus.OK)
    public  @ResponseBody Genre getGenreById(@PathVariable Integer id) {
        Genre genre = genreService.getGenre(id);
        return genre;
    }


    @PutMapping(value = "/editGenre/{id}")
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody void editGenre(@PathVariable Integer id, @Valid @RequestBody Genre genre ) {
        genreService.saveGenres(genre);
    }

}
