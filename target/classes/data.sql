delete from genre;
delete from book;

INSERT INTO genre(id, name) VALUES (1000, 'Romance');
INSERT INTO genre(id, name) VALUES (1001, 'Fantasia');
INSERT INTO genre(id, name) VALUES (1002, 'Contemporaneo');
INSERT INTO genre(id, name) VALUES (1003, 'Drama');
INSERT INTO genre(id, name) VALUES (1004, 'Aventura');
INSERT INTO genre(id, name) VALUES (1005, 'Accion');


INSERT INTO book(id,title,author,description,status,language,genre_id) VALUES (2000, 'Carry On','Raindow Rowell','Popular','Publicado','Ingles',1000);
INSERT INTO book(id,title,author,description,status,language,genre_id) VALUES (2001, 'Fangirl','Raindow Rowell','Popular','Publicado','Ingles',1000);
INSERT INTO book(id,title,author,description,status,language,genre_id) VALUES (2002, 'Despues de clases','Carla Angelo','Bueno','Publicado','Español',1001);
INSERT INTO book(id,title,author,description,status,language,genre_id) VALUES (2003, 'Heaven official''s Blessing','Mo Xiang Tong Xiu','Ficcion Historica','Publicado','Chino',1003);

